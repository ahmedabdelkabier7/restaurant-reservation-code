package com.resturant;

import org.w3c.dom.Node;

import javax.xml.bind.Element;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(name = "dish")
//@XmlAccessorType(XmlAccessType.FIELD)
public class Dish {
   // @XmlElement(name = "name")
    private String dishName;
    private int amount;
  //  @XmlElement(name = "price")
    private float price;
   // @XmlElement(name = "type")
    private String type;

    public Dish(String dishName, String type, float price ,int amount) {    //list of creation
        this.dishName = dishName;
        this.amount = amount;
        this.price=price;
        this.type=type;
    }

   public Dish(String element1, String element2, String element3) { //file load data from input
        this.dishName = element1;
       this.price = Float.parseFloat(element2);
       this.type = element3;
  }

    public Dish(String dishname, String dishtype, String price, String amount) {  //for order file data
        this.dishName = dishname;
        this.amount = Integer.parseInt(amount);
        this.price= Float.parseFloat(price);
        this.type=dishtype;
    }

    public String getDishName() {
        return dishName;
    }
    public int getAmount() {
        return amount;
    }

    public float getPrice() {
        return price;
    }

    public String getType() {return type;}
}
