package com.resturant;

import java.util.ArrayList;

public class Cooker extends User {
    public void showOrderCooker(ArrayList<Orders> orders) {
        //load data from file
        for (Orders orders1 : orders) {
            System.out.println("CustomerTable: " + orders1.getTableNumber());
            for (Dish dish1 : orders1.getDishes())
                System.out.println("Dishes Choised\n" + dish1.getDishName() + " X" + dish1.getAmount());
        }
    }

    public Cooker(String element1, String element2, String element3, String element4) {
        super(element1, element2, element3, element4);
    }
}
