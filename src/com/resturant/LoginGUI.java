package com.resturant;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LoginGUI extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Restaurant Reservation");
        primaryStage.setWidth(320);             primaryStage.setHeight(250);
        VBox parent = new VBox();
        Label label = new Label("UserName");
        TextField userName = new TextField();
        userName.setMaxWidth(200);
        Label label1 = new Label("Password");
        TextField password = new TextField();
        password.setMaxWidth(200);
        Button login = new Button("LOGIN");
        login.setPrefSize(100,50);
        parent.getChildren().addAll(label,userName,label1,password,login);
        Scene scene = new Scene(parent);
        primaryStage.setScene(scene);
        primaryStage.show();
        login.setOnAction(e->{

        });
    }
}
