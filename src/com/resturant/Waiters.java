package com.resturant;

import java.util.ArrayList;

public class Waiters extends User {
    public void showOrderWaiter(ArrayList<Orders> orders) {
        //load data from file
        for (Orders orders1 : orders) {
            System.out.println("CustomerName: " + orders1.getCustomerName() + "\n" + "CustomerTable: " + orders1.getTableNumber());
        }
    }

    public Waiters(String element1, String element2, String element3, String element4) {
        super(element1, element2, element3, element4);
    }
}
