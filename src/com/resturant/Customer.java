package com.resturant;

import java.util.ArrayList;

public class Customer extends User {
    public void addDishes(String dishName,int amount,float price, ArrayList<Dish> dishes,String type)
    {
            dishes.add(new Dish(dishName, type, price,amount));  //type of dish increase in the constractor
    }
    public void saveOrder(String customerName, int tableNumber, ArrayList<Dish> dishes, ArrayList<Orders> orders) {

        orders.add(new Orders(dishes, customerName, tableNumber));
        //add order list in XML file
    }
    public void showOrderCustomer(ArrayList<Orders> orders){
        //load data from file
            for (Orders orders1:orders) {
                 System.out.println("CustomerName: "+orders1.getCustomerName() + "\n" +"CustomerTable: "+orders1.getTableNumber());
            for (Dish dish1 :orders1.getDishes())
                System.out.println("Dishes Choised\n"+ dish1.getDishName()+" X"+dish1.getAmount()+ "\nType: "+dish1.getType()+"\tPrice: "+ dish1.getPrice());
            System.out.println("Cashout: "+orders1.getCashOut());
        }
    }

    public Customer(String element1, String element2, String element3, String element4) {
        super(element1, element2, element3, element4);
    }
}
