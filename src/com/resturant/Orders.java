package com.resturant;

import org.w3c.dom.Node;

import java.util.ArrayList;

public class Orders{
   private ArrayList<Dish> dishes;
   private String customerName;
   private int tableNumber;
   private  float cashOut;     //calculated when file loaded by name of dish and amount taken from customer we load dish's price and * by amount

   public Orders(ArrayList<Dish> dishes, String customerName, int tableNumber) {    //to fill array list of orders from user
      this.dishes = dishes;
      this.customerName = customerName;
      this.tableNumber = tableNumber;
      cashOutCalculation();
   }

   public Orders(ArrayList<Dish> dishes, String userName, String tableNumber, String cashout) {
      this.dishes = dishes;
      this.customerName = userName;
      this.tableNumber = Integer.parseInt(tableNumber);
      this.cashOut= Float.parseFloat(cashout);
   }


   private void cashOutCalculation(){
      for (Dish dish1 :dishes)
         cashOut+= dish1.getAmount()* dish1.getPrice();
   }
   public ArrayList<Dish> getDishes() {
      return dishes;
   }

   public String getCustomerName() {
      return customerName;
   }

   public int getTableNumber() {
      return tableNumber;
   }
   public float getCashOut() {
      return cashOut;
   }

}
