package com.resturant;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Data {
   private ArrayList<Table>tables = new ArrayList<>();
   private ArrayList<User>users = new ArrayList<>();
    private ArrayList<Dish> dishes = new ArrayList<>();
    private Document doc;
    public Data() throws ParserConfigurationException, IOException, SAXException {
        File file = new File("C:\\Users\\AhmadAbdulkabier\\IdeaProjects\\ResturantReservationSystem\\src\\com\\resturant\\input.XML");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(file);
        doc.getDocumentElement().normalize();
        loadDishes(doc);
        loadUsers(doc);
        loadTables(doc);
    }

    private void loadDishes(Document doc) {

        NodeList nodeList = doc.getElementsByTagName("dish");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element eElemet = (Element)node;
                dishes.add(new Dish(eElemet.getElementsByTagName("name").item(0).getTextContent(),eElemet.getElementsByTagName("price").item(0).getTextContent(),eElemet.getElementsByTagName("type").item(0).getTextContent()));
            }
        }
    }
    private void loadUsers(Document doc){

        NodeList nodeList = doc.getElementsByTagName("user");
        for (int i=0;i<nodeList.getLength();i++)
        {
            Node node = nodeList.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element eElemet = (Element)node;
                if(eElemet.getElementsByTagName("role").item(0).getTextContent().equals("Client"))
                    users.add(new Customer(eElemet.getElementsByTagName("name").item(0).getTextContent(),eElemet.getElementsByTagName("role").item(0).getTextContent(),eElemet.getElementsByTagName("username").item(0).getTextContent(),eElemet.getElementsByTagName("password").item(0).getTextContent()));
                else if (eElemet.getElementsByTagName("role").item(0).getTextContent().equals("Manager"))
                    users.add(new Manager(eElemet.getElementsByTagName("name").item(0).getTextContent(),eElemet.getElementsByTagName("role").item(0).getTextContent(),eElemet.getElementsByTagName("username").item(0).getTextContent(),eElemet.getElementsByTagName("password").item(0).getTextContent()));
                else if (eElemet.getElementsByTagName("role").item(0).getTextContent().equals("Waiter"))
                    users.add(new Waiters(eElemet.getElementsByTagName("name").item(0).getTextContent(),eElemet.getElementsByTagName("role").item(0).getTextContent(),eElemet.getElementsByTagName("username").item(0).getTextContent(),eElemet.getElementsByTagName("password").item(0).getTextContent()));
                else
                    users.add(new Cooker(eElemet.getElementsByTagName("name").item(0).getTextContent(),eElemet.getElementsByTagName("role").item(0).getTextContent(),eElemet.getElementsByTagName("username").item(0).getTextContent(),eElemet.getElementsByTagName("password").item(0).getTextContent()));
            }
        }
    }
    private void loadTables(Document doc){
        NodeList nodeList = doc.getElementsByTagName("table");
        for (int i=0;i<nodeList.getLength();i++)
        {
            Node node = nodeList.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element eElemet = (Element)node;
                    tables.add(new Table(eElemet.getElementsByTagName("number").item(0).getTextContent(),eElemet.getElementsByTagName("number_of_seats").item(0).getTextContent(),eElemet.getElementsByTagName("smoking").item(0).getTextContent()));
            }
        }
    }
    public ArrayList<Dish> getLoadedDishes(){
        //for(Dish dish:dishes)
         //   System.out.println("Name: "+dish.getDishName()+"\nType: "+dish.getType()+"\nPrice: "+dish.getPrice()+"\n");
        return dishes;
    }
    public ArrayList<Table> getLoadedTables(){
        //for(Table table:tables)
         //   System.out.println("Number: "+table.getTableNumber()+"\nNumber Of Seats: "+table.getNumberOfSeats()+"\nIn Smoking Area: "+table.isSmoking()+"\n");
        return tables;
    }
    public ArrayList<User> getLoadedUsers(){
    //        for(User user:users)
    //            System.out.println("Name: "+user.getName()+"\nRole: "+user.getRole()+"\nUser Name: "+user.getUserName()+"\nPassword: "+user.getPassword()+"\n");
            return users;
    }

}
