package com.resturant;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class WriteXMLOrder {
    private ArrayList<Orders>orders = new ArrayList<>();
    private ArrayList<Dish>dishes = new ArrayList<>();
    public WriteXMLOrder() throws IOException, SAXException, ParserConfigurationException {
        getOrder();
    }

    public void saveOrder(ArrayList<Orders>orders) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.newDocument();
        Element rootElement = doc.createElement("Orders");
        doc.appendChild(rootElement);
        for (Orders orders1:orders){
        rootElement.appendChild(getOrder(doc, orders1.getCustomerName(), orders1.getTableNumber(), orders1.getCashOut(),orders1.getDishes()));
//            for (Dish dish:orders1.getDishes())
//                rootElement.appendChild(getDishes(doc,dish.getDishName(),dish.getType(),dish.getAmount(),dish.getPrice()));

        }
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        //for pretty print
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(doc);

        //write to console or file

        StreamResult file = new StreamResult(new File("C:\\Users\\AhmadAbdulkabier\\IdeaProjects\\ResturantReservationSystem\\src\\com\\resturant\\orders.XML"));

        //write data

        transformer.transform(source, file);
        System.out.println("DONE");


    }


    private static Node getOrder(Document doc, String username, int tablenumber, double cashout,ArrayList<Dish>dishes5)
    {
        Element order = doc.createElement("order");
        Element dishes = doc.createElement("dishes");
        Element dish = doc.createElement("dish");
        //set id attribute

        order.appendChild(getOrderElements(doc, order, "name", username));

        order.appendChild(getOrderElements(doc, order, "tablenumber", String.valueOf(tablenumber)));

        order.appendChild(getOrderElements(doc, order, "cashout", String.valueOf(cashout)));
        order.appendChild(dishes);
        for (Dish dish1:dishes5)
            dishes.appendChild(getDishes(doc,dish1.getDishName(),dish1.getType(),dish1.getAmount(),dish1.getPrice()));


        return order;
    }

    private static Node getDishes(Document doc, String dishName,String dishType,int amount , float price)
    {
        Element dishes = doc.createElement("dishes");
        Element dish = doc.createElement("dish");
        //dishes.appendChild(getOrderElements(doc, dishes, "dishes", ""));
        //dishes.appendChild(dish);
        dish.appendChild(getOrderElements(doc, dish, "dishname", dishName));

        dish.appendChild(getOrderElements(doc, dish, "dishtype", dishType));

        dish.appendChild(getOrderElements(doc, dish, "amount", String.valueOf(amount)));

        dish.appendChild(getOrderElements(doc, dish, "price", String.valueOf(price)));


        return dish;
    }


    private static Node getOrderElements(Document doc, Element element, String name, String value) {        // append name & tablenumber...
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }

    private void getOrder() throws ParserConfigurationException, IOException, SAXException {
        File file = new File("C:\\Users\\AhmadAbdulkabier\\IdeaProjects\\ResturantReservationSystem\\src\\com\\resturant\\orders.XML");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(file);
        doc.getDocumentElement().normalize();
        NodeList nodeList1 = doc.getElementsByTagName("dish");
        for (int i=0;i<nodeList1.getLength();i++)
        {
            Node node = nodeList1.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element eElemet = (Element)node;
                dishes.add(new Dish(eElemet.getElementsByTagName("dishname").item(0).getTextContent(),eElemet.getElementsByTagName("dishtype").item(0).getTextContent(),eElemet.getElementsByTagName("price").item(0).getTextContent(),eElemet.getElementsByTagName("amount").item(0).getTextContent()));
            }
        }
        NodeList nodeList2 = doc.getElementsByTagName("order");
        for (int i=0;i<nodeList2.getLength();i++)
        {
            Node node = nodeList2.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element eElemet = (Element)node;
                orders.add(new Orders(dishes,eElemet.getElementsByTagName("name").item(0).getTextContent(),eElemet.getElementsByTagName("tablenumber").item(0).getTextContent(),eElemet.getElementsByTagName("cashout").item(0).getTextContent()));
            }
        }
    }
    public ArrayList<Orders> getCustomerOrder(){
        return orders;
    }
}
