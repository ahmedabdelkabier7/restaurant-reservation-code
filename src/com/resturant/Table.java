package com.resturant;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(name = "table")
//@XmlAccessorType(XmlAccessType.FIELD)
public class Table {
   // @XmlElement(name = "number")
    private int tableNumber;
  //  @XmlElement(name = "number_of_seats")
    private int numberOfSeats;
   // @XmlElement(name = "smoking")
    private String  isSmoking;

    public Table(int tableNumber, int numberOfSeats, String isSmoking) {
        this.tableNumber = tableNumber;
        this.numberOfSeats = numberOfSeats;
        this.isSmoking = isSmoking;
    }
    public Table(String element1, String element2, String element3) {
        this.tableNumber =Integer.parseInt(element1);
        this.numberOfSeats = Integer.parseInt(element2);
        this.isSmoking =element3;
    }



    public int getTableNumber() {
        return tableNumber;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public String isSmoking() {
        return isSmoking;
    }
}
