package com.resturant;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class User {
    @XmlElement(name = "username")
    private String userName;
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "password")
    private String password;
    @XmlElement(name = "role")
    private String role;

    public User(String element1, String element2, String element3, String element4) {
        setName(element1);
        setRole(element2);
        setUserName(element3);
        setPassword(element4);
    }

    public String getRole() {return role;}
    public void setRole(String role) {this.role = role;}
    public String getUserName() {return userName;}
    private void setUserName(String userName) {this.userName = userName;}
    public String getName() {return name;}
    private void setName(String name) {this.name = name;}
    public String getPassword() {return password;}
    private void setPassword(String password) {this.password = password;}
    public void creatNewUser(String name,String userName,String password,String role){
        setName(name);
        setUserName(userName);
        setPassword(password);
        setRole(role);
    }
}

